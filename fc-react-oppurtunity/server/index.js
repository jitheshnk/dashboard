const express = require('express');
const mysql = require('mysql');
const app = express();
require('dotenv').config();
const createquery = require('./model')
const connection = mysql.createConnection(
    {
        
        host:process.env.HOST,
        user:process.env.USERS,
        password:process.env.PASSWORD,
        database:process.env.DATABASE

    }
    
);
connection.connect(err => {
    if (err){
        return err;
    }
})

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/getdata', (req,res) => {
    
    
    queryAll = createquery(req);
    
    
    
    
    connection.query(queryAll,(err,response) =>{
        if(err){
            return res.send(err);
        }
        else{
            res.send(response).json;
        }
    })
    
});

const PORT = "8000";
app.listen(PORT,() => {
    console.log(`listening port ${PORT}`)
});