var createQuery = function(req){

 let filterCondition = '';
    let query=req.query;
    let queryAll= '';
    if(query.init == 1 || query.init == undefined){
        queryAll = `SELECT DISTINCT Salesperson,Country FROM Sales_Team__Performance_data `;
        
    }
    else{
    
        if(query.startDate || query.endDate || query.employees){
            filterCondition = ' where ';
        }
        if(query.startDate && query.endDate){
            if(query.startDate > query.endDate){
                filterCondition += `year(Date_Created_New) between '${query.startDate}' AND '${parseInt(query.startDate)}'`;
            }
            else{
                filterCondition += `year(Date_Created_New) between '${query.startDate}' AND '${parseInt(query.endDate)}'`;
            }
        }
        
        if( query.employees){
            if(query.startDate){
                filterCondition+=' AND ';
            }
            filterCondition += `Salesperson = '${query.employees}'`;
        }
        queryAll =`SELECT ID,Closed,Country,Account_Industry,Date_Closed_New,Date_Created_New,Region,Opportunity_Max_Amount,Salesperson FROM Sales_Team__Performance_data ${filterCondition}`;
    }
    return queryAll;
}
module.exports=createQuery;