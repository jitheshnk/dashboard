import React, { Component } from 'react';
import ReactLoading from 'react-loading';
import FusionCharts from 'fusioncharts';
import Zoomline from 'fusioncharts/fusioncharts.zoomline';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import ReactFC from 'react-fusioncharts';
import createZoomLineJson from './chartConfigOpportunityWinRate';
ReactFC.fcRoot(FusionCharts, Zoomline, FusionTheme);

class OpportunityWinRate extends Component{
  constructor(props){
    super(props);
    this.chartConfig={};
  }
  
  
  render () {
    let chartData = this.props.chartData
    this.chartConfig = createZoomLineJson(chartData);  
   if(chartData['date']!==""){ 
      return (
          <ReactFC type="zoomlinedy" width="100%" height="300px" dataFormat="json" dataSource={this.chartConfig.dataSource}/>
        )
      }
      else
      return (<div className="chartdiv"><ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} /></div>)
  }
}

  
     
export default OpportunityWinRate;
