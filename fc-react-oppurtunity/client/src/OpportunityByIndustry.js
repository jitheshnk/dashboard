//Chart1 : 
import React, { Component } from 'react';
import ReactLoading from 'react-loading';
import FusionCharts from 'fusioncharts';
import TreeMap from 'fusioncharts/fusioncharts.treemap';
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import createChartjson from './chartConfigOppotunityByIndustry';

ReactFC.fcRoot(FusionCharts, TreeMap, FusionTheme);

class OpportunityByIndustry extends Component{
    constructor(){
        super();
        this.state={
            "selected":'',
            "data":[]
        };
        this.chartConfigs={};
        this.treeData=[];
        this.maxValue=null;
        this.minValue=null;
        
    }
    
    dataSource(){
        
        let data=this.props.chartData;
        let i=0,opportunity;
        
        for(let industry in data)
        {   
            this.treeData[i++]={
            "label": industry,
            "value": data[industry]["Opportunity"],
            "sValue": data[industry]["Win"],
            }
            if(this.maxValue===null || this.maxValue<data[industry]["Win"]){
                this.maxValue=data[industry]["Win"]
            }
            if(this.minValue===null || this.minValue>data[industry]["Win"]){
                this.minValue=data[industry]["Win"]
            }
        }
       
        this.chartConfigs = createChartjson(this.treeData,this.maxValue,this.minValue);
    }
    render () {         
        this.dataSource();
        if(this.treeData.length>0){
            return (
                
            <div className="p-4">
            <ReactFC {...this.chartConfigs } />
            </div>
            )
        }
        else{
            return (<div className="chartdiv"><ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} /></div>)
        }
    }
}

export default OpportunityByIndustry;
