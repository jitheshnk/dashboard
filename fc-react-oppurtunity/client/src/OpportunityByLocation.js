import React, { Component } from 'react';
import ReactLoading from 'react-loading';
import FusionCharts from 'fusioncharts';
import Maps from 'fusioncharts/fusioncharts.maps';
import World from 'fusionmaps/maps/fusioncharts.worldwithcountries';
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import createChartConfig from './chartConfigOpportunityByLocation';
 
ReactFC.fcRoot(FusionCharts, Maps, World, FusionTheme);


class OpportunityLocation extends Component{
    constructor(props){
        super(props);
        this.locations=[];
        this.map ={
            "AE":{
                "id": "28",
                "shapeid": "myCustomShape",
                "x": "1328.6",
                "y": "574",
                "label": "Abu Dhabi - UnitedArabEmirates",
                "tooltext":"Average opportunity amount in UAE is <b>$value<b> ",
                "labelpos": "right"
            },
            "AF":{
                "id": "42",
                "shapeid": "myCustomShape",
                "x": "1402.41",
                "y": "507.81",
                "label": "Kabul - Afghanistan",
                "tooltext":"Average opportunity amount in Afghanistan is <b>$value<b> ",
                "labelpos": "bottom"
            },
            // "AL":"Albania",
            // "AM": "Armenia",
            // "AR": "ARGENTINA",
            // "AT":"Austria",
            "AU":{
                "id": "Aus",
                "shapeid": "myCustomShape",
                "x": "1790.14",
                "y": "875.9",
                "label": "Autralia",
                
                "tooltext": "In Australia, WeWork has <b>$value</b> co-working locations"
              },
            // "BB":"Barbados",
            // "BD":"Bangladesh",
            // "BE":"Belgium",
            // "BG":"Bulgaria",
            // "BH":"Bhutan",
            // "BM":"Bermuda",
            // "BO":"Bolivia",
            "BR":{
                "id": "57",
                "shapeid": "myCustomShape",
                "x": "713.87",
                "y": "796.72",
                "label": "Brazil",
                "tooltext":"Average opportunity amount in Brazil is <b>$value<b> ",
                "labelpos": "right"
            },
            // "CA":"Canada",
            // "CD":"DR Congo",
            // "CG":"Congo",
            // "CH":"Switzerland",
            "CL":{
                "id": "lon",
                "shapeid": "we-anchor",
                "x": "190.23",
                "y": "350.9",
                "label": "Chile",
                "value": "1",
                "tooltext": "In Chile, WeWork has <b>$value</b> co-working location"
              },
            //"CM":"Cameroon",
            "CN":{
                "id": "china",
                "shapeid": "we-anchor",
                "x": "573.14",
                "y": "161.9",
                "label": "China",
                "value": "6",
                "tooltext": "In China, WeWork has <b>$value</b> co-working locations"
              },
            "CO":{
                "id": "52",
                "shapeid": "myCustomShape",
                "x": "559.87",
                "y": "698.72",
                "label": "Colombia",
                "tooltext":"Average opportunity amount in Colombia is <b>$value<b> ",
                "labelpos": "bottom"
            },
            // "CR":"Costa Rica",
            // "CY":"Cyprus",
            // "CZ":"Czech Republic",
            // "DE":"Germany",
            // "DK":"Denmark",
            // "DO":"Dominican Republic",
            // "DZ":"Algeria",
            // "EC":"Ecuador",
            // "EG":"Egypt",
            "ES":{
                "id": "spain",
                "shapeid": "we-anchor",
                "x": "330.14",
                "y": "145.9",
                "label": "Spain",
                "value": "2",
                "tooltext": "In Spain, WeWork has <b>$value</b> co-working locations"
              },
            "ET":{
                "id": "22",
                "shapeid": "myCustomShape",
                "x": "1233.14",
                "y": "664.36",
                "label": "Ethiopia",
                "tooltext":"Average opportunity amount in Ethiopia is <b>$value<b> ",
                "labelpos": "bottom"
            },
            // "FI":"Finland",
            // "FO": "",
            // "FR":"France",
            // "GA":"Gabon",
            // "GB":"",
            // "GH":"Ghana",
            // "GI":"Gibraltor",
            // "GR":"Greece",
            // "GT":"Guatemala",
            // "GY":"Guyana",
            // "HK":"Hong Kong",
            // "HN":"Honduras",
            // "HR":"",
            // "HU":"Hungary",
            "ID":{
                "id": "Indo",
                "shapeid": "we-anchor",
                "x": "570.14",
                "y": "250.9",
                "label": "Indonesia",
                "value": "1",
                "tooltext": "In Indonesia, WeWork has <b>$value</b> co-working location"
              },
            //"IE":"",
            "IN":{
                "id": "ind",
                "shapeid": "myCustomShape",
                "x": "1452.87",
                "y": "600.18",
                "label": "India",
                "tooltext": "In India, WeWork has <b>$value</b> co-working locations",
                "labelpos": "bottom"
              },
            "IS":{
                "id": "isrl",
                "shapeid": "we-anchor",
                "x": "445.14",
                "y": "165.9",
                "label": "Isreal",
                "value": "5",
                "tooltext": "In Israel, WeWork has <b>$value</b> co-working locations"
              },
            // "IT":"Italy",
            // "JM":"Jamaica",
            "JP":{
                "id": "jap",
                "shapeid": "we-anchor",
                "x": "633.14",
                "y": "145.9",
                "label": "Japan",
                "value": "1",
                "tooltext": "In Japan, WeWork has <b>$value</b> co-working location"
              },
            // "KE":"Kenya",
            "KR":{
                "id": "sKorea",
                "shapeid": "we-anchor",
                "x": "603.14",
                "y": "155.9",
                "label": "South Korea",
                "value": "1",
                "tooltext": "In South Korea, WeWork has <b>$value</b> co-working location"
              },
            // "KW":"",
            // "KY":"Cayman Islands",
            // "LB":"Lebanon",
            // "LK":"Sri Lanka",
            // "LT":"Lithuania",
            // "LU":"Luxembourg",
            // "MA":"Morocco",
            // "MH":"Marshall Islands",
            // "MT":"Malta",
            // "MU":"",
            // "MV":"Moldova",
            "MX":{
                "id": "03",
                "shapeid": "myCustomShape",
                "x": "441.5",
                "y": "618.54",
                "label": "Mexico",
                "tooltext":"Average opportunity amount in Mexico is <b>$value<b> ",
                "labelpos": "left"
            },
            // "MY":"Malaysia",
            // "NA":"Namibia",
            // "NC":"New Caledonia",
            // "NI":"Nicaragua",
            // "NL":"Netherlands",
            // "NO":"Norway",
            // "NZ":"New Zealand",
            "OM":{
                "id": "27",
                "shapeid": "myCustomShape",
                "x": "1340.05",
                "y": "591.81",
                "label": "Oman",
                "tooltext":"Average opportunity amount in Oman is <b>$value<b> ",
                "labelpos": "right"
            },
            // "PA":"Panama",
            // "PE":"Peru",
            // "PG":"Papua New Guinea",
            "PH":{
                "id": "06",
                "shapeid": "myCustomShape",
                "x": "1715.5",
                "y": "640.18",
                "label": "Philippines",
                "tooltext":"Average opportunity amount in Philippines is <b>$value<b> ",
                "labelpos": "left"
            },
            "PK":{
                "id": "33",
                "shapeid": "myCustomShape",
                "x": "1427.87",
                "y": "525.63",
                "label": "Pakistan",
                "tooltext":"Average opportunity amount in Pakistan is <b>$value<b> ",
                "labelpos": "bottom"
            },
            "PL":{
                "id": "pol",
                "shapeid": "we-anchor",
                "x": "365.14",
                "y": "118.9",
                "label": "Poland",
                "value": "1",
                "tooltext": "In Poland, WeWork has <b>$value</b> co-working location"
              },
            // "PR":"Puerto Rico",
            // "PT":"Portugal",
            "PY":{
                "id": "47",
                "shapeid": "myCustomShape",
                "x": "664.23",
                "y": "874.36",
                "label": "Paraguay",
                "tooltext":"Average opportunity amount in Paraguay is <b>$value<b> ",
                "labelpos": "right"
            },
            "QA":{
                "id": "29",
                "shapeid": "myCustomShape",
                "x": "1308.23",
                "y": "575.27",
                "label": "Qatar",
                "tooltext":"Average opportunity amount in Qatar is <b>$value<b> ",
                "labelpos": "bottom"
            },
            // "RO":"Romania",
            // "RS":"",
            "RU":{
                "id": "36",
                "shapeid": "myCustomShape",
                "x": "1596.96",
                "y": "317.63",
                "label": "Russia",
                "tooltext":"Average opportunity amount in Russia is <b>$value<b> ",
                "labelpos": "right"
            },
            // "SA":"Saudi Arabia",
            // "SE":"Sweden",
            "SG":{
                "id": "Sing",
                "shapeid": "we-anchor",
                "x": "560.14",
                "y": "231.9",
                "label": "Singapore",
                "value": "1",
                "tooltext": "In Singapore, WeWork has <b>$value</b> co-working location"
              },
            // "SI":"Slovenia",
            // "SK":"Slovakia",
            // "SR":"Suriname",
            // "SV":"Svalbard",
            "TH":{
                "id": "Thi",
                "shapeid": "we-anchor",
                "x": "553.14",
                "y": "211.9",
                "label": "Thailand",
                "value": "1",
                "tooltext": "In Thailand, WeWork has <b>$value</b> co-working location"
              },
            //"TN":"Tunisia",
            //"TR":"",
            //"TT":"Trinidad & Tobago",
            "TW":{
                "id": "05",
                "shapeid": "myCustomShape",
                "x": "1719.32",
                "y": "576.54",
                "tooltext":"Average opportunity amount in Taiwan is <b>$value<b> ",
                "label": "Taiwan"
            },
            // "UA":"Ukraine",
            "US":{
                "id": "atl",
                "shapeid": "we-anchor",
                "x": "130.14",
                "y": "140.9",
                "label": "USA",
                "value": "25",
                "tooltext": "In USA, WeWork has <b>$value</b> co-working locations</b>",
                "labelpos": "left"
              },
            "UY":{
                "id": "46",
                "shapeid": "myCustomShape",
                "x": "678.23",
                "y": "931.63",
                "label": "Uruguay",
                "tooltext":"Average opportunity amount in Uruguay is <b>$value<b> ",
                "labelpos": "right"
            },
            // "VE":"Venezuela",
            // "VN":"Vietnam",
            "ZA":{
                "id": "10",
                "shapeid": "myCustomShape",
                "x": "1114.78",
                "y": "932.9",
                "label": "South Africa",
                "tooltext":"Average opportunity amount in South Africa is <b>$value<b> ",
                "labelpos": "left"
            },
            // "ZM":"Zambia",
            // "ZW":"Zimbabwe"
        };
        this.chartConfigs={};
        
    }
    
    formatData(){
        const { mapData}  = this.props;
        for (var items in mapData){
            if(this.map[items]){

                this.map[items]["tooltext"]="<b>$label</b> :<br>Opportunity Amount: $";
                if(mapData[items]['opportunity'] >= 1000000000 && mapData[items]['opportunity'] <= 999999999999){
                    this.map[items]["tooltext"]+= Math.round( (mapData[items]['opportunity']/1000000000 )) + "B";
                }
                else if(mapData[items]['opportunity'] >= 1000000 && mapData[items]['opportunity'] <= 999999999){
                    this.map[items]["tooltext"]+= Math.round( (mapData[items]['opportunity']/1000000 ) ) + "M";
                }
                else if(mapData[items]['opportunity'] >= 1000 && mapData[items]['opportunity'] <= 999999){
                    this.map[items]["tooltext"]+=  Math.round( (mapData[items]['opportunity']/1000 ) ) + "K";
                }
                this.map[items]["tooltext"]+="<br> Total Opportunities: "+mapData[items]['totalDeal'];
                this.map[items]["value"]=mapData[items]['opportunity'];

                this.locations.push(this.map[items]);
            }
        }
        this.chartConfigs = createChartConfig(this.locations);
        
        // let mapDataSource={
        //     "map": {
        //     "caption": "Opportunity by Location",
        //     "captionPadding": "30",
        //     "showshadow": "0",
        //     "showlabels": "0",
        //     "showmarkerlabels": "0",
        //     "fillcolor": "F1f1f1",
        //     "bordercolor": "CCCCCC",
        //     "basefont": "Verdana",
        //     "basefontsize": "10",
        //     "markerbordercolor": "000000",
        //     "markerbgcolor": "FF5904",
        //     "markerradius": "6",
        //     "usehovercolor": "0",
        //     "hoveronempty": "0",
        //     "showmarkertooltip": "1",
        //     "canvasBorderColor": "375277",
        //     "canvasBorderAlpha": "0"
        //     },
        //     "markers": {
        //     "shapes": [
        //         {
        //             "id": "myCustomShape",
        //             "type": "circle",
        //             "fillcolor": "FFFFFF,29C3BE",
        //             "fillpattern": "radial",
        //             "showborder": "0",
        //             "radius": "4"
        //         },
        //         {
        //             "id": "newCustomShape",
        //             "type": "circle",
        //             "fillcolor": "FFFFFF,000099",
        //             "fillpattern": "radial",
        //             "showborder": "0",
        //             "radius": "3"
        //         }
        //     ],
        //     "items": this.locations
        //     }
        // };

        // this.chartConfigs={ type: "maps/worldwithcountries",
        //     renderAt: "chart2",
        //     width: "100%",
        //     height: "300px",
        //     dataFormat: "json",
        //     dataSource: mapDataSource 
        //     };

    }
    render () {
        this.formatData();
        if(this.locations.length){ 
            return (
                <div className="p-4">
                <ReactFC {...this.chartConfigs } />
                </div>
            )
        }
        else{
            return(
                <div className="chartdiv"><ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} /></div>
            )
        }
    }
    
}
 
export default OpportunityLocation;
