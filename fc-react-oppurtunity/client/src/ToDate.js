import React, {Component} from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
//import axios from 'axios';

class ToDate extends Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.yearTill = 0;
        this.yearRange = [];
        this.state = {
            selectedYear:'',
            dropdownOpen: false,
            menuSelected:[]
        };

    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen

        });
    }
    
    selectedYear =(selected) =>{
        const presentState = this.state;
        presentState.selectedYear = selected;
        this.setState(presentState);
        this.props.onFilterSelect(this.state.selectedYear);
    }
    createYearRange(years){
        this.yearRange=[];
        if(this.yearTill === 0 && parseInt(years[1])){
            this.yearTill = parseInt(years[1]);
        }
        for (var i=parseInt(years[0]) ; i<=  this.yearTill ; i++){
            this.yearRange[i]=i;
        }
    }
    
    render(){
        const { years} = this.props;  
        this.createYearRange(years);
        let renderyears = this.yearRange.map(year => { return (<DropdownItem className="dropdownfocus" onClick={() => this.selectedYear(year)}> {year}</DropdownItem>) });
        
        if(years[1]){
            if(!this.state.selectedYear){
                let state = this.state;
                state.selectedYear = years[1];
                DropdownToggle['data-toggle']=years[1];
                this.setState(state); 
            }
            return (
                
                <ButtonDropdown  isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle className="dropdownNameSize dropdownButton pt-0" caret>
                    {this.state.selectedYear}
                </DropdownToggle>
                <DropdownMenu 
                    modifiers={{
                        setMaxHeight: {
                        enabled: true,
                        order: 890,
                        fn: (data) => {
                            return {
                            ...data,
                            styles: {
                                ...data.styles,
                                overflow: 'auto',
                                maxHeight: 200,
                            },
                            };
                        },
                        },
                    }}
                    >
                    {/* <DropdownItem header>Region</DropdownItem> */}
                    {renderyears}
                </DropdownMenu>
            </ButtonDropdown>
            );
        }
        else{
            return(<ButtonDropdown  isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle className="dropdownNameSize dropdownButton pt-0" caret>
                  Year
                </DropdownToggle>
                <DropdownMenu>
                  
                </DropdownMenu>
            </ButtonDropdown>
            )

        }

    }

}





export default ToDate;