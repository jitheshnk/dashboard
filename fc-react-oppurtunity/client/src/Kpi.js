import React, {Component} from 'react';
import { Card, Row,Col } from 'reactstrap';
import ReactLoading from 'react-loading';

class Kpi extends Component{
    constructor(){
        super();
    }
    truncateAmount(amt){
        amt["avgClosureDays"]=Math.round( amt["avgClosureDays"]);
        amt["totalDeals"]=Math.round (amt["totalDeals"] );  
        if(amt["totalOpportunity"] >= 1000000000 && amt["totalOpportunity"] <= 999999999999){
            amt["totalOpportunity"]= Math.round( (amt["totalOpportunity"]/1000000000 ));
            return " B ";
        }
        else if(amt["totalOpportunity"] >= 1000000 && amt["totalOpportunity"] <= 999999999){
            amt["totalOpportunity"]= Math.round( (amt["totalOpportunity"]/1000000 ) );
            return " M ";
        }
        else if(amt["totalOpportunity"] >= 1000 && amt["totalOpportunity"] <= 999999){
            amt["totalOpportunity"]=  Math.round( (amt["totalOpportunity"]/1000 ) ) ;
            return " K ";
        }
        

    }
    
    render(){
        let data = this.props.kpiData;
        let unit = this.truncateAmount(data);
        if(data["avgWinRate"]!==""){
            return (
                <div>
                    <Row className= "pt-4">
                        <Col md="3" sm="6" className="mt-4 mb-4 ">
                            <Card className="shadow border-0 bg-white kpi-card">
                                <div className= "kpi p-2"><p className="kpi-heading">Total Opportunity</p>
                                    <p className= "kpi-value"><span className="kpi-unit"> </span> {data["totalDeals"]}</p></div>
                            </Card>
                        </Col>
                        <Col md="3" sm="6" className="mt-4 mb-4">
                            <Card className="shadow border-0 bg-white kpi-card">
                            <div className= "kpi p-2">
                                <p className="kpi-heading">Total Opportunity Amount</p>
                                <p className= "kpi-value"> ${data["totalOpportunity"]}<span className="kpi-unit">{unit}</span></p></div>
                            </Card>
                        </Col>
                        <Col md="3" sm="6" className="mt-4 mb-4">
                            <Card className="shadow border-0 bg-white kpi-card">
                            <div className = "kpi p-2"><p className="kpi-heading">WinRate </p>
                            <p className= "kpi-value">{data["avgWinRate"]}<span className="kpi-unit">%</span></p></div>
                            </Card>
                        </Col>
                        <Col md="3" sm="6" className="mt-4 mb-4">
                            <Card className="shadow border-0 bg-white kpi-card">
                            <div className="kpi p-2"><p className="kpi-heading">Average Deal Closure</p>
                            <p className= "kpi-value">{data["avgClosureDays"]}<span className="kpi-unit">days</span></p></div>
                            </Card>
                        </Col>
                    </Row>
                </div>
            );
        }
        else{ 
            return (
                <div>
                    <Row className= "pt-4">
                        <Col md="3" sm="6" className="mt-4 mb-4 ">
                            <Card className="shadow border-0 bg-white kpi-card">
                                <div className= "kpi kpi-loading p-2">
                                    <ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} />
                                </div>
                            </Card>
                        </Col>
                        <Col md="3" sm="6" className="mt-4 mb-4">
                            <Card className="shadow border-0 bg-white kpi-card">
                            <div className= "kpi kpi-loading p-2">
                                <ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} />
                            </div>
                            </Card>
                        </Col>
                        <Col md="3" sm="6" className="mt-4 mb-4">
                            <Card className="shadow border-0 bg-white kpi-card">
                            <div className = "kpi kpi-loading p-2">
                                <ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} />
                            </div>
                            </Card>
                        </Col>
                        <Col md="3" sm="6" className="mt-4 mb-4">
                            <Card className="shadow border-0 bg-white kpi-card">
                            <div className="kpi kpi-loading p-2">
                                <ReactLoading  className="chartLoading" type={"bars"} color={"black"} height={'10%'} width={'10%'} />
                            </div>
                            </Card>
                        </Col>
                    </Row>
                </div>
            );

        }
    }
}
export default Kpi;