var createZoomLineJson = function (data){
    const msCombi = {
      "chart": {
        "caption": "Opportunity & Win Rate",
        "basefont": "Helvetica Neue",
        "showToolTip":"1",
        "useCrossLine":"0",
        "captionPadding": "30",
        "pYAxisName": "Opportunity Max Amount (in $)",
        "sYAxisName": "Win Rate %",
        "compactDataMode": "1",
        "pixelsPerPoint": "0",
        "lineThickness": "1",
        "dataSeparator": "|",
        "numberPrefix": "$",
        "snumberSuffix": "%",
        "pYAxisMaxValue": "1",
        "pYAxisMinValue": "9",
        "sYAxisMaxValue": "100",
        "sYAxisMinValue": "0",
        "theme": "fint"
      },
      "categories": [{
        "category": data['date']
      }],
      "dataset": [{
        "seriesname": data['dataset1']['seriesname'],
        "data": data['dataset1']['data'],
      }, {
        "seriesname": data['dataset2']['seriesname'],
        "parentYAxis": "S",
        "data": data['dataset2']['data'],
      }]
    }
    let chartConfig = {
      type: "zoomlinedy",
      width: "100%",
      height: "300px",
      dataFormat: "json",
      dataSource: msCombi
    };

    return chartConfig;
  }
  export default createZoomLineJson;