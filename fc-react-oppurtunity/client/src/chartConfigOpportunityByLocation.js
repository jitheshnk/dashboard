var createChartConfig = function(locations){ 
    let mapDataSource={
        "map": {
        "caption": "Opportunity by Location",
        "captionPadding": "30",
        "showshadow": "0",
        "showlabels": "0",
        "showtooltip": "0",
        "showmarkerlabels": "0",
        "fillcolor": "F1f1f1",
        "bordercolor": "CCCCCC",
        "basefont": "Helvetica Neue",
        "markerbordercolor": "000000",
        "markerbgcolor": "FF5904",
        "markerradius": "6",
        "usehovercolor": "0",
        "hoveronempty": "0",
        "showmarkertooltip": "1",
        "canvasBorderColor": "375277",
        "canvasBorderAlpha": "0",
        "numberScaleValue": "1,1000,1000",
        "numberPrefix":"$",
        "numberScaleUnit": "K,M,B",
        },
        "markers": {
        "shapes": [
            {
                "id": "myCustomShape",
                "type": "circle",
                "fillcolor": "FFFFFF,29C3BE",
                "fillpattern": "radial",
                "showborder": "0",
                "radius": "4",
                "fillalpha":"15,60"
            },
            {
                "id": "newCustomShape",
                "type": "circle",
                "fillcolor": "FFFFFF,000099",
                "fillpattern": "radial",
                "showborder": "0",
                "radius": "3",
                "opacity":"0.5"
            }
        ],
        "items": locations
        }
    };


    let chartConfigs={ type: "maps/worldwithcountries",
        renderAt: "chart2",
        width: "100%",
        height: "300px",
        dataFormat: "json",
        dataSource: mapDataSource 
    };

    return chartConfigs;
}
export default createChartConfig;