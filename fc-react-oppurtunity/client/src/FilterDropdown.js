import React, {Component} from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';


class FilterDropdown extends Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          cSelected:[],
          rSelected:{},
          dropdownOpen: false,
          menuSelected:[]
        };

      }
      toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen

        });
      }
    
    countrySelected= function(selected) {
      const selectedCountries = this.state.cSelected;
      const index = selectedCountries.indexOf(selected);
      if (index < 0) {
        selectedCountries.push(selected);
      } else {
        selectedCountries.splice(index, 1);
      }
      this.setState({ cSelected: [...selectedCountries] });
      this.props.onFilterSelect(null,this.state.cSelected);
    };
    
    regionSelected =(selected) =>{
      const presentState = this.state;
      if(presentState.rSelected.selected){
        presentState.rSelected.relected=null;
      } 
      else{
        presentState.rSelected=selected;
      }
      this.setState(presentState);
      this.props.onFilterSelect(this.state.rSelected,null);
    }
    
    render(){
      const { regionMenu, countryMenu } = this.props;  
      const renderRegion = Object.keys(regionMenu).map(region => { return (<DropdownItem  onClick={() => this.regionSelected(region)}> {region}</DropdownItem>) });
      const renderCountry = Object.keys(countryMenu).map(country => { return (<DropdownItem value={country} onClick={() => this.countrySelected(country)}>{country}</DropdownItem>) });
      return (
        
        <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
          <DropdownToggle caret>
            Location
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem header>Region</DropdownItem>
             {renderRegion}
            <DropdownItem divider />
            <DropdownItem header>Countries</DropdownItem>
            {renderCountry}
          </DropdownMenu>
      </ButtonDropdown>
    );

    }

}





export default FilterDropdown;