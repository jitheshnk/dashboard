import React, { Component } from 'react';
import OpportunityWinRate  from './OpportunityWinRate';
import OpportunityByLocation from './OpportunityByLocation';
import OpportunityByIndustry from './OpportunityByIndustry';
import axios from 'axios';
import { BrowserRouter } from 'react-router-dom';
import { Card, Container, Row,Col } from 'reactstrap';
import SalesPersonDropdown from './SalesPersonDropdown';
import FromDate from './FromDate';
import ToDate from './ToDate';
import Kpi from './Kpi';

class App extends Component {
  constructor(){
    super();
    this.data = [];
    this.countries=[];
    this.salesPersons = [];
    this.region={};
    this.kpi = {
      totalDeals:'',
      totalOpportunity:'',
      avgClosureDays:'',
      avgWinRate:''
    };
    this.state = {
      dataFetched:false,
      salesPerson:[],
      filter:{
       'init':1,
        'startDate':'',
        'endDate':'',
        'employees':''
      },
      chart1:{
        date:'',
        dataset1:{
          seriesname:'',
          data:''
        },
        dataset2:{
          seriesname:'',
          data:''
        }
      },
      chart2:{},
      chart3:{}
    };
   // this.getData();
    
  }
  async getData(){
    
    const res = await axios.get("https://www.fusioncharts.com/demos/dashboards/api/sales-opportunity-dashboard/getdata",{params:this.state.filter});
    
    this.data=res.data;
    if(this.state['filter']['employees']!==""){
      this.formatData();
    }
    else{
      let state =  this.state;
      state["filter"]["employees"] = this.data[0]['Salesperson'];
      this.setState(state);
    }
  }
  
  async componentDidMount(){
    let state=this.state;
    
    if(this.state["filter"]["init"]==1){
      await this.getData().then(state.filter.init=0);
    }
    this.setState(state);
    await this.getCountries();
    await this.getData();
    
  }

  getCountries(){
    let salesPersons=[];
    for( var i = 0 ; i < this.data.length;i++){
      
      if(salesPersons.indexOf(this.data[i]['Salesperson']) === -1) {
        salesPersons.push(this.data[i]['Salesperson']);
      }
      
    }
    let state=this.state;
    state.salesPerson=salesPersons;
    
    this.setState(state);
  }
 
  
  handleLocation = (region,country) => {
    let state = Object.assign({},this.state);
    let filter = state.filter;
    if(region){
      filter["region"]=region;
    }
    else if(this.state.filter.countries[country]){
      filter.countries[country]=null;
    }
    else{
      filter.countries=country;
      
    }
    this.setState(state);
    this.getData();
    
  };

  filterBySelectedPerson = (person) => {
    let state = Object.assign({},this.state);
    let filter = state.filter;
    filter["employees"]=person;
    this.setState(state);
    this.getData();
  }

  filterByStartDate = (year) => {
    let state = Object.assign({},this.state);
    let filter = state.filter;
    filter["startDate"]=year;
    this.setState(state);
    this.getData();
  }

  filterByEndDate = (year) => {
    let state = Object.assign({},this.state);
    let filter = state.filter;
    filter["endDate"]=year;
    this.setState(state);
    this.getData();
  }

  handleDate = (sDate,eDate) => {
    let state = this.state;
    state.filter.startDate = new Date(sDate);
    state.filter.endDate = new Date (eDate);
    this.setState(state);
    this.getData();
    
  }

  formatData(){ 
    let data=this.data,date = null,year=null,month = 0,winrate=0,opportunity = 0, dateIterated = 0,opportunityAmt = 0, closureDay=0,deals=0,closedDate=0,win=0,perYeardata=[],
    treeData={},options = {  year: 'numeric', month: 'numeric', day: 'numeric' },createdDate=0,countries={},startYear=null,endYear=null,
    zoomLineData={date:'',
    dataset1:{
      seriesname:'',
      data:''
    },
    dataset2:{
      seriesname:'',
      data:''
    }};
    zoomLineData["dataset1"]["seriesname"]="Opportunity Max Amount";
    zoomLineData["dataset2"]["seriesname"]="Win Rate";
    
    for(let i=0; i<data.length; i++){   
      let industry= data[i]["Account_Industry"];
      treeData[industry]={
        "Opportunity": treeData[industry] ? parseFloat(treeData[industry]["Opportunity"])  + parseFloat( data[i]["Opportunity_Max_Amount"] )  : parseFloat(data[i]["Opportunity_Max_Amount"]) ,
          "Win": treeData[industry] ? ( ((  parseInt(treeData[industry]["deals"]) * parseInt( treeData[industry]["Win"] ) ) + parseInt(data[i]["Closed"]=="Yes"?100:0) ) / (parseInt(treeData[industry]["deals"])+1) ): data[i]["Closed"]=="Yes"?100:0,
        "deals": treeData[industry] ? parseInt(treeData[industry]["deals"]) + 1 :1
      }
      
      treeData[industry]["Opportunity"] = (Math.round(treeData[industry]["Opportunity"] * 100) )/100;
      treeData[industry]["Win"] = (Math.round(treeData[industry]["Win"] * 100) )/100;
      closedDate =  new Date(data[i]["Date_Closed_New"]);
      createdDate = new Date(data[i]["Date_Created_New"]);
      dateIterated= new Date(data[i]["Date_Created_New"]);
      startYear = startYear ? (startYear < createdDate.getFullYear() ? startYear : createdDate.getFullYear() ) : createdDate.getFullYear();
      endYear = endYear ? (endYear > createdDate.getFullYear() ? endYear : createdDate.getFullYear() ) : createdDate.getFullYear();
      if(year === null){
        date=new Date(data[i]["Date_Created_New"]);
        year=date.getFullYear();
        perYeardata =[{},{},{},{},{}];
        month = dateIterated.getMonth()%4 ;
        perYeardata[month]= {"opp": parseFloat(data[i]["Opportunity_Max_Amount"]),
      "win": data[i]["Closed"]=="Yes"?1:0,
      "deals": 1};
        deals=deals+1;
      }
      else if(year===dateIterated.getFullYear()){
       
        month =  dateIterated.getMonth()%4 ;
        perYeardata[month]= {
          "opp": perYeardata[month] && perYeardata[month]["opp"] ?( parseFloat(perYeardata[month]["opp"])+ parseFloat(data[i]["Opportunity_Max_Amount"]) ) : parseFloat(data[i]["Opportunity_Max_Amount"]),
          "win": perYeardata[month] && perYeardata[month]["win"] ? parseFloat(perYeardata[month]["win"]) + parseInt(data[i]["Closed"]=="Yes"?1:0) : parseInt(data[i]["Closed"]=="Yes"?1:0),
        "deals": perYeardata[month] && perYeardata[month]["deals"] ? parseInt(perYeardata[month]["deals"]) + 1 : 1};

          if(i===data.length-1){
            winrate = (perYeardata[1] && perYeardata[1]["win"] ? (perYeardata[1]["win"]/ perYeardata[1]["deals"])*100 : 0)+"|"+(perYeardata[2] && perYeardata[2]["win"] ? (perYeardata[2]["win"]/perYeardata[2]["deals"])*100 : 0) +"|"+(perYeardata[3] && perYeardata[3]["win"] ? (perYeardata[3]["win"] /perYeardata[3]["deals"])*100: 0)+"|"+(perYeardata[0] && perYeardata[0]["win"] ? (perYeardata[0]["win"] / perYeardata[0]["deals"])*100 : 0) +"|" ;
            opportunity = (perYeardata[1] && perYeardata[1]["opp"] ? (Math.round(perYeardata[1]["opp"] *100))/100 : 0)+"|"+(perYeardata[2] && perYeardata[2]["opp"] ? (Math.round(perYeardata[2]["opp"] * 100))/100 : 0) +"|"+(perYeardata[3] && perYeardata[3]["opp"] ? (Math.round(perYeardata[3]["opp"] *100 ))/100: 0)+"|"+(perYeardata[0] && perYeardata[0]["opp"] ? (Math.round(perYeardata[0]["opp"]*100))/100 : 0)+"|";
            zoomLineData['date'] = zoomLineData['date']?zoomLineData['date'] + "Q1,"+year+"|Q2,"+year+"|Q3,"+year+"|Q4,"+year+"|" : "Q1,"+year+"|Q2,"+year+"|Q3,"+year+"|Q4,"+year+"|";
            zoomLineData["dataset1"]['data'] = zoomLineData["dataset1"]['data'] ? (zoomLineData["dataset1"]['data'] + opportunity) : opportunity
            zoomLineData["dataset2"]['data'] = zoomLineData["dataset2"]['data'] ? (zoomLineData["dataset2"]['data'] + winrate) : winrate;
            year=dateIterated.getFullYear();
            month = dateIterated.getMonth()%4 ;
            perYeardata[month]= {"opp": parseFloat(data[i]["Opportunity_Max_Amount"]),
          "win": data[i]["Closed"]=="Yes"?1:0,
          "deals": 1};
          }
      }
      else{ 
        winrate = (perYeardata[1] && perYeardata[1]["win"] ? (perYeardata[1]["win"]/ perYeardata[1]["deals"])*100 : 0)+"|"+(perYeardata[2] && perYeardata[2]["win"] ? (perYeardata[2]["win"]/perYeardata[2]["deals"])*100 : 0) +"|"+(perYeardata[3] && perYeardata[3]["win"] ? (perYeardata[3]["win"] /perYeardata[3]["deals"])*100: 0)+"|"+(perYeardata[0] && perYeardata[0]["win"] ? (perYeardata[0]["win"] / perYeardata[0]["deals"])*100 : 0) +"|" ;
        opportunity = (perYeardata[1] && perYeardata[1]["opp"] ? (Math.round(perYeardata[1]["opp"] *100))/100 : 0)+"|"+(perYeardata[2] && perYeardata[2]["opp"] ? (Math.round(perYeardata[2]["opp"] * 100))/100 : 0) +"|"+(perYeardata[3] && perYeardata[3]["opp"] ? (Math.round(perYeardata[3]["opp"] *100 ))/100: 0)+"|"+(perYeardata[0] && perYeardata[0]["opp"] ? (Math.round(perYeardata[0]["opp"]*100))/100 : 0)+"|";
        zoomLineData['date'] = zoomLineData['date']?zoomLineData['date'] + "Q1,"+year+"|Q2,"+year+"|Q3,"+year+"|Q4,"+year+"|" : "Q1,"+year+"|Q2,"+year+"|Q3,"+year+"|Q4,"+year+"|";
        zoomLineData["dataset1"]['data'] = zoomLineData["dataset1"]['data'] ? (zoomLineData["dataset1"]['data'] + opportunity) : opportunity
        zoomLineData["dataset2"]['data'] = zoomLineData["dataset2"]['data'] ? (zoomLineData["dataset2"]['data'] + winrate) : winrate;
        year=dateIterated.getFullYear();
        month = dateIterated.getMonth()%4 ;
        perYeardata[month]= {"opp": parseFloat(data[i]["Opportunity_Max_Amount"]),
      "win": data[i]["Closed"]=="Yes"?1:0,
      "deals": 1};
        
      }
      
      opportunityAmt += parseFloat(data[i]["Opportunity_Max_Amount"]);
      
      closureDay += Math.floor((Date.UTC(closedDate.getFullYear(), closedDate.getMonth(), closedDate.getDate()) -  (Date.UTC(createdDate.getFullYear(), createdDate.getMonth(), createdDate.getDate()) )) /(1000 * 60 * 60 * 24));
      win += data[i]["Closed"]!=="No"?1:0;

      if(!countries[this.data[i]["Country"]] ){
        countries[(this.data[i]["Country"]).toUpperCase()]={'location':(this.data[i]["Country"]).toUpperCase(),
        'opportunity':Math.round( parseFloat(this.data[i]['Opportunity_Max_Amount']) * 100 )/100,
        'totalDeal': 1 };
      }
      else{
        countries[this.data[i]["Country"]]['opportunity']+=Math.round( parseFloat(this.data[i]['Opportunity_Max_Amount']) * 100)/100;
        countries[this.data[i]["Country"]]['totalDeal']=parseInt(countries[this.data[i]["Country"]]['totalDeal'])+1;
        
      }
    }
    zoomLineData["dataset1"]['data']=zoomLineData["dataset1"]["data"].substring(0, zoomLineData["dataset1"]['data'].length - 1);
    zoomLineData["dataset2"]['data']=zoomLineData["dataset2"]["data"].substring(0, zoomLineData["dataset2"]['data'].length - 1);
    this.kpi["totalDeals"]=this.data.length;
    this.kpi["totalOpportunity"] = Math.round( (opportunityAmt) * 100)/100;
    this.kpi["avgClosureDays"] = Math.round( (closureDay/this.data.length) * 100)/100;
    this.kpi["avgWinRate" ]= Math.round( (win*100/this.data.length) *100)/100;
    let states=this.state;
    states.chart1= zoomLineData;
    states.chart2=countries;
    states.chart3=treeData;
    states.filter.startDate=startYear;
    states.filter.endDate=endYear;
    this.setState(states);
  }

  render() {
    return (
      <BrowserRouter basename="/sales-opportunity-dashboard">
        <div className="App base-color base-font">
        <div className="wrapper">
          <div className="heading">
          <Container>
            <Row>
              <Col className="pr-0">
                <div className="heading-wrapper">
                  <div className="header-label mt-0 mb-0">Sales Opportunity Dashboard of </div>
                  <SalesPersonDropdown  className="pt-0 mt-0"  salesPerson={this.state.salesPerson} onFilterSelect={this.filterBySelectedPerson}/>
                  <div className="yearRange-wrapper">
                    <div className="yearRange-label mt-0 mb-0">From</div>
                    <FromDate className="pt-0 mt-0"  years={[this.state.filter.startDate,this.state.filter.endDate]} onFilterSelect={this.filterByStartDate}/>
                    <div className="yearRange-label mt-0 mb-0">To</div>
                    <ToDate className="pt-0 mt-0"  years={[this.state.filter.startDate,this.state.filter.endDate]} onFilterSelect={this.filterByEndDate}/>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
          </div>
        </div> 
        
        <Container>
        
          
            <div className="pt-2">
              
              <Row>
                <Col sm="12" >
                  
                </Col>
                
              </Row>
            </div>
            <div className="">
              <Kpi kpiData={this.kpi}/>
            </div>
            <div className="">
                <Row>
                  <Col sm="12" className="p-4">
                    <Card className="shadow border-0 p-4 rounded">
                      <OpportunityWinRate className="p-4" chartData={this.state.chart1} />
                    </Card>
                  </Col>
                </Row> 
              </div> 
            <div className="pb-4">
              <Row>
              <Col md="6" sm="12" className="mt-4 mb-4">
                <Card className="shadow border-0 bg-white rounded">
                  <OpportunityByLocation mapData={this.state.chart2} />
                </Card>
              </Col>
              <Col md="6" sm="12" className="mt-4 mb-4">
                <Card className="shadow border-0 bg-white rounded">
                  <OpportunityByIndustry chartData={ this.state.chart3 }/>
                </Card>
              </Col>
            </Row>
            </div>
          </Container>        
        </div>
        </BrowserRouter>
    );
  }

}

export default App;
