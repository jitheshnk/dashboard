import React, {Component} from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
//import axios from 'axios';

class SalesPersonDropdown extends Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          selectedPerson:'',
          dropdownOpen: false,
          menuSelected:[]
        };

    }
    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen

        });
    }
    
    selectedPerson =(selected) =>{
        const presentState = this.state;
        presentState.selectedPerson = selected;
        this.setState(presentState);
        this.props.onFilterSelect(this.state.selectedPerson);
    }
    
    render(){
        const { salesPerson} = this.props;  
        const renderSalesPerson = salesPerson.map(person => { return (<DropdownItem  onClick={() => this.selectedPerson(person)}> {person}</DropdownItem>) });
        if(salesPerson.length>=1){
            if(!this.state.selectedPerson){
                let state = this.state;
                state.selectedPerson = salesPerson[0];
                DropdownToggle['data-toggle']=salesPerson[0];
                this.setState(state); 
            }
            return (
                
                <ButtonDropdown  isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle className="dropdownNameSize dropdownButton pt-0" caret>
                    {this.state.selectedPerson}
                </DropdownToggle>
                <DropdownMenu 
                    modifiers={{
                        setMaxHeight: {
                        enabled: true,
                        order: 890,
                        fn: (data) => {
                            return {
                            ...data,
                            styles: {
                                ...data.styles,
                                overflow: 'auto',
                                maxHeight: 200,
                            },
                            };
                        },
                        },
                    }}
                    >
                    {/* <DropdownItem header>Region</DropdownItem> */}
                    {renderSalesPerson}
                </DropdownMenu>
            </ButtonDropdown>
            );
        }
        else{
            return(<ButtonDropdown  isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle className="dropdownNameSize dropdownButton pt-0" caret>
                  Sales Person
                </DropdownToggle>
                <DropdownMenu>
                  
                </DropdownMenu>
            </ButtonDropdown>
            )

        }

    }

}





export default SalesPersonDropdown;