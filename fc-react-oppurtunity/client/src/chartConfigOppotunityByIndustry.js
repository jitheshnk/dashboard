var createChartjson = function (treeData,maxValue,minValue){
    const treemapDataSource = {
        "chart": {
          "caption": "Opportunity by Industry",
          "captionPadding": "20",
          "basefont": "Helvetica Neue",
          "animation": "0",
          "hideTitle": "0",
          "plotToolText": "<div><b>$label</b><br/> Opportunity Amount: $$datavalue<br/>Win Rate: $svalue%</div>",
          "spacex": "0",
          "spacey": "0",
          "horizontalPadding": "1",
          "verticalPadding": "1",
          "hoveronallsiblingleaves": "1",
          "borderColor":"ffffff",
          "bgColor":"ffffff",
          "plotborderthickness": ".5",
          "plotbordercolor": "666666",
          "legendpadding": "0",
          "legendItemFontSize": "10",
          "legendItemFontBold": "1",
          "showLegend": "1",
          "legendPointerWidth": "1",
          "legendBgColor":"ffffff",
          "legendBorderColor":"ffffff",
          "legenditemfontcolor": "3d5c5c",
          "algorithm": "squarified",
          "legendScaleLineThickness": "0",
          "legendCaptionFontSize": "10",
          "legendaxisbordercolor": "bfbfbf",
          "legendCaption": "WIN RATE",
          "legendspreadfactor": "1",
          "showParent": "0",
          "showBorder": "0",
          "bgColor": "FFFFFF,",
          "numberScaleValue": "1,1000,1000",
          "numberScaleUnit": "K,M,B"
          //"theme": "fusion"
        },
        "data": [{'data':treeData}],
        "colorrange": {
            "mapbypercent": "1",
            "gradient": "1",
            "minvalue": minValue,
            "code": "#FFFFFF",
            
            "color": [{
              "code": "#29C3BE",
              "maxvalue": maxValue
              
            }]
          }
        }
    
        let chartConfigs = {
        type: "treemap",
        renderAt: "chart3",
        width: "100%",
        height: "300px",
        dataFormat: "json",
        dataSource: treemapDataSource
        };
    
    return chartConfigs;

}
export default createChartjson;